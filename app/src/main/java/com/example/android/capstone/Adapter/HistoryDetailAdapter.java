package com.example.android.capstone.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.capstone.R;

/**
 * Created by parth on 13/2/18.
 */

public class HistoryDetailAdapter extends CursorAdapter{

    public HistoryDetailAdapter(Context context, Cursor c){
        super(context,c);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.history_item,parent,false);
        return view;
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView mUserName=view.findViewById(R.id.history_user_name);
        TextView mDate=view.findViewById(R.id.history_date);
        TextView mFrom=view.findViewById(R.id.history_from);
        TextView mTo=view.findViewById(R.id.history_to);
        TextView mDept=view.findViewById(R.id.history_dep);
        TextView mArr=view.findViewById(R.id.history_arr);
        TextView mBusType=view.findViewById(R.id.history_bustype);
        TextView mSeatNo=view.findViewById(R.id.history_seatno);
        TextView mFare=view.findViewById(R.id.history_fare);

        mUserName.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1))));
        mDate.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2))));
        mFrom.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(3))));
        mTo.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(4))));
        mDept.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(5))));
        mArr.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(6))));
        mBusType.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(7)))+"("+cursor.getString(cursor.getColumnIndex(cursor.getColumnName(8)))+")");
        mSeatNo.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(9))));
        mFare.setText(cursor.getString(cursor.getColumnIndex(cursor.getColumnName(10))));
    }

}
