package com.example.android.capstone.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.capstone.R;

public class BusDetailAdapter extends CursorAdapter{
    String bus_name;
    public BusDetailAdapter(Context context, Cursor c){
        super(context,c);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.single_bus_detail,parent,false);
        return view;
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView busNameText=view.findViewById(R.id.bus_name);
        TextView arr_timeText=view.findViewById(R.id.arr_time);
        TextView depart_timeText=view.findViewById(R.id.dept_time);
        TextView fareText=view.findViewById(R.id.fare);
        bus_name=cursor.getString(cursor.getColumnIndex(cursor.getColumnName(3)));
        busNameText.setText(bus_name+" (Type: "+cursor.getString(cursor.getColumnIndex(cursor.getColumnName(2)))+")");
        arr_timeText.setText("ARR TIME : "+cursor.getString(cursor.getColumnIndex(cursor.getColumnName(7))));
        depart_timeText.setText("DEP TIME : "+cursor.getString(cursor.getColumnIndex(cursor.getColumnName(6))));
        fareText.setText("Fare : "+cursor.getString(cursor.getColumnIndex(cursor.getColumnName(8))));
    }

}
