package com.example.android.capstone.Activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionForBusBooking;
import com.example.android.capstone.SessionForCity;
import com.example.android.capstone.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SeaterActivity extends AppCompatActivity implements View.OnClickListener {

    ArrayList<String> Seat=new ArrayList<String>();
    List<String> BookedSeat=new ArrayList<String>();
    Button mBook;
    SessionForBusBooking mBusSession;
    SessionForCity mCitySession;
    SessionManager userSession;
    DatabaseHelper dB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seater);
        mBusSession=new SessionForBusBooking(this);
        mCitySession=new SessionForCity(this);
        userSession=new SessionManager(this);
        dB =new DatabaseHelper(this);

        String bookedSeat=dB.getBookedSeat(mBusSession.getDate(),mBusSession.getRouteID());
        if(bookedSeat!=null) {
            BookedSeat= Arrays.asList(bookedSeat.split("\\s*,\\s*"));
        }
        String Id=userSession.getUserId();
        String Name= dB.fetchUserName(Id);

        userSession.setName(Name);
        final Intent intent=getIntent();
        final String ROUTE_ID=intent.getStringExtra("Bus_id");
        final int price_Seat=intent.getIntExtra("price_seat",0);
        mBook=findViewById(R.id.book_seat);
        TableLayout t1;
        t1=findViewById(R.id.main_table);
        int count=1;
        for(int i=1;i<=8;i++) {
            TableRow tr = new TableRow(this);
            TableLayout.LayoutParams params = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT);
            tr.setLayoutParams(params);
            for (int j = 1; j <= 5; j++) {
                Button seat_btn = new Button(this);
                TableRow.LayoutParams seat_params = new TableRow.LayoutParams(140, 140);
                seat_params.setMargins(30, 10, 0, 10);
                seat_btn.setLayoutParams(seat_params);
                seat_btn.setGravity(Gravity.CENTER);
            //    seat_btn.setBackgroundColor(getResources().getColor(R.color.remains));
                seat_btn.setOnClickListener(this);
                if (j == 3 && i != 8) {
                    seat_btn.setVisibility(View.INVISIBLE);
                } else {
                    seat_btn.setText(String.valueOf(count));
                    seat_btn.setId(count);
                    count++;
                    if(BookedSeat.contains(seat_btn.getText())){
                        seat_btn.setBackgroundColor(getResources().getColor(R.color.confirm));
                    }
                    else{
                        seat_btn.setBackgroundColor(getResources().getColor(R.color.remains));
                    }
                }
                seat_btn.setPadding(0, 0, 0, 0);
                tr.addView(seat_btn);
            }
            t1.addView(tr);
        }
        mBook.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Integer booking_seat=Seat.size();
            String Date=mBusSession.getDate();
            if(!Seat.isEmpty()){
                String Seat_no=Seat.toString().replace("[","").replace("]","").trim();

                String FARE=String.valueOf(booking_seat*price_Seat);
                boolean Book= dB.insertBookData(ROUTE_ID,userSession.getUserId(),Date,Seat_no,booking_seat,FARE);
                if(Book==true){
                    Intent intent1=new Intent(SeaterActivity.this,TripHistoryActivity.class);
                    startActivity(intent1);
                    finish();
                }
            }
          else{
                Toast.makeText(SeaterActivity.this, "Can not Book", Toast.LENGTH_SHORT).show();
          }
        }
    });
    }
    @Override
    public void onClick(View v) {
            int id=v.getId();
        //    Button clickedSeat=findViewById(v.getId());
            //Drawable buttonBackground=clickedSeat.getBackground();
            ColorDrawable seatColor= (ColorDrawable) v.getBackground();
            int ColorId=seatColor.getColor();
            if(ColorId==getResources().getColor(R.color.select)) {
                v.setBackgroundColor(getResources().getColor(R.color.remains));
                Seat.remove(String.valueOf(id));
            }
            else if(ColorId==getResources().getColor(R.color.confirm)){
                Toast.makeText(this,"Already booked",Toast.LENGTH_SHORT).show();
            }
            else{
            v.setBackgroundColor(getResources().getColor(R.color.select));
            Seat.add(String.valueOf(id));
            }
    }
}
