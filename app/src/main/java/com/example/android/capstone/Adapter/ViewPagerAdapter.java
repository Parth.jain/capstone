package com.example.android.capstone.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.android.capstone.Fragments.LowerSeatFragment;
import com.example.android.capstone.Fragments.UpperSeatFragment;

/**
 * Created by parth on 13/12/17.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new UpperSeatFragment();
        } else if (position == 1) {
            fragment = new LowerSeatFragment();
        }
        return fragment;
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Upper";
        } else if (position == 1) {
            title = "Lower";
        }
        return title;
    }
}
