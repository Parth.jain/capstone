package com.example.android.capstone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.android.capstone.Activities.LoginActivity;

/**
 * Created by parth on 8/2/18.
 */

public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    int PRIVATE_MODE=0;

    private static final String PREF_NAME="Login_Pref";

    private static final String IS_LOGIN="IsLoggedIn";

    private static final String KEY_ID="ID";

    private static final String NAME="NAME";

    private static final String EMAIL="EMAIL";

    public SessionManager(Context context){
        this.context=context;
        pref=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=pref.edit();
    }

    public void createLoginSession(String ID){

        editor.putBoolean(IS_LOGIN,true);
        editor.putString(KEY_ID,ID);
        editor.commit();

    }
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
    public String getUserId(){
        String id=pref.getString(KEY_ID,null);
        return id;
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    public void LogoutUser(){
        editor.clear();
        editor.commit();


    }
    public void setName(String name){
        editor.putString(NAME,name);
        editor.commit();
    }
    public String getName(){
       String name=pref.getString(NAME,null);
        return name;
    }

    public void setUserEmail(String email) {
        editor.putString(EMAIL,email);
        editor.commit();
    }
    public String getEmail(){
        String email=pref.getString(EMAIL,null);
        return email;
    }
}
