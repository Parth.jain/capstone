package com.example.android.capstone.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RegisterUserActivity extends AppCompatActivity  {

    Spinner mSelectCity;
    Button mRegister;
    EditText mFullName,mBirthDate,mEmailId,mPassword;
    String city,emailId,password,fullName,birthDate;
    private DatabaseHelper myDb;
    Calendar myCalendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        myDb = new DatabaseHelper(this);
        mEmailId=findViewById(R.id.editEmailId);
        mBirthDate=findViewById(R.id.editBirthDate);
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)           {
                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
               /* updateLabel();*/
                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                Date date=myCalendar.getTime();
                if(date.after(Calendar.getInstance().getTime())){
                    Toast.makeText(RegisterUserActivity.this, "Can not Select this Date As Birthdate, Please choose Another!", Toast.LENGTH_SHORT).show();
                    mBirthDate.setText("");
                }
                else{
                    mBirthDate.setText(sdf.format(myCalendar.getTime()));}
            }
        };
        mBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(RegisterUserActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mFullName=findViewById(R.id.Name);
        mPassword=findViewById(R.id.editPassword);
        mSelectCity=findViewById(R.id.spinnerCity);
        mRegister=findViewById(R.id.btnRegister);
        List<String> cities=new ArrayList<String>();
        cities.add("Ahmedabad");
        cities.add("Baroda");
        cities.add("Surat");
        cities.add("Rajkot");
        cities.add("Junagadh");
        ArrayAdapter<String> spinAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,cities);

        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        mSelectCity.setAdapter(spinAdapter);

        mSelectCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position,
            long id) {
                city=parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailId=mEmailId.getText().toString();
                birthDate=mBirthDate.getText().toString();
                password=mPassword.getText().toString();
                fullName=mFullName.getText().toString();
                city=mSelectCity.getSelectedItem().toString();
                boolean registered=checkRegistered(emailId);
                boolean validation=checkValidation(emailId,birthDate,password,fullName,city);
                boolean emailValidation=checkEmailpattern(emailId);
                if(registered==false && validation==true && emailValidation==true)
                {

                    RegisterUser(fullName, birthDate, city, emailId, password);
                    Toast.makeText(RegisterUserActivity.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                    Intent intent =new Intent(RegisterUserActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();

                }
                else if(registered==true) {
                    clearAll();
                    Toast.makeText(RegisterUserActivity.this, "User Already registered, Try with new Email id", Toast.LENGTH_SHORT).show();
                    // myDb.insertData(fullName,birthDate,city,emailId,password);
                }
            }
        });
    }

    private boolean checkValidation(String emailId, String birthDate, String password, String fullName, String city) {
        if(!emailId.equals("") && !birthDate.equals("") && !password.equals("")&& !fullName.equals("") && !city.equals("")){
            return true;
        }
        else{

            Toast.makeText(this, "Enter Valid data", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void clearAll() {
        mBirthDate.setText("");
        mPassword.setText("");
        mFullName.setText("");
        mEmailId.setText("");
    }
    private boolean checkEmailpattern(String emailId){
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if(emailId.matches(emailPattern)){
         return true;
        }
        else {
            Toast.makeText(this, "Enter valid Email", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    private boolean checkRegistered(String emailId) {

        boolean registered=myDb.checkUser(emailId);

        return registered;
    }

    private void RegisterUser(String fullName, String birthDate, String city, String emailId, String password) {
        boolean isInserted=myDb.insertData(fullName,birthDate,city,emailId,password);
        if(isInserted==true) {
            Log.e("Data", "Entered Successfully");
        }
        else{
            Log.e("Data","Insertion failed");
        }
    }

}
