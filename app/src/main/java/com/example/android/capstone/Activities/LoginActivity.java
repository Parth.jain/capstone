package com.example.android.capstone.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView mRegisterText,mForgotPassText;
    EditText mEditEmail,mEditPass;
    Button mLoginBtn;
    DatabaseHelper myDb;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        myDb = new DatabaseHelper(this);
        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn()==true){
            Intent i=new Intent(LoginActivity.this,SearchNavActivity.class);
            startActivity(i);
            finish();
        }
        else {
            mForgotPassText=findViewById(R.id.textForgotpass);
            mForgotPassText.setOnClickListener(this);
            mRegisterText = findViewById(R.id.textRegister);
            mRegisterText.setOnClickListener(this);
            mEditEmail = findViewById(R.id.editEmail);
            mEditPass = findViewById(R.id.editPassword);
            mLoginBtn = findViewById(R.id.btnLogin);

            mLoginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String email = mEditEmail.getText().toString();
                    String pass = mEditPass.getText().toString();
                    Cursor cursor = myDb.checkLogin(email, pass);
                    String Id;
                    if (cursor != null && cursor.moveToFirst()) {
                        Id = cursor.getString(cursor.getColumnIndex("ID"));
                        cursor.close();
                        Intent successLogin = new Intent(LoginActivity.this, SearchNavActivity.class);
                        session.createLoginSession(Id);
                        String Name=myDb.fetchUserName(Id);
                        session.setName(Name);
                        String emailId=myDb.fetchEmailId(Id);
                        session.setUserEmail(emailId);
                        successLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(successLogin);

                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Enter correct Email id and Password", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }
    @Override
    public void onClick(View v) {
         if(v.getId()==R.id.textRegister){
             Intent intent=new Intent(this,RegisterUserActivity.class);
             startActivity(intent);
         }
         else if(v.getId()==R.id.textForgotpass){
             forgotPassword();
         }
    }

    private void forgotPassword() {

        LayoutInflater li=LayoutInflater.from(this);
        final View promptView=li.inflate(R.layout.enterpass,null);
        AlertDialog.Builder alertDialogBuilder=new AlertDialog.Builder(this);

        alertDialogBuilder.setView(promptView);

        final EditText mForgotEmail=promptView.findViewById(R.id.enterEmail);

        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
           getPassword(mForgotEmail.getText().toString());
            }
        });
        AlertDialog alertDialog=alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     *
     * @param emailId-useful for getting password using the EmailId.
     */
    private void getPassword(String emailId) {

        String Password = null;
        boolean checkEmail=myDb.checkUser(emailId);
        if(checkEmail==true)
        {
            Password = myDb.fetchPassword(emailId);
        }
        else{
            Password="Not Found! Check Entered Mail";
        }
        AlertDialog.Builder alertDialogBuild = new AlertDialog.Builder(this);
        alertDialogBuild.setTitle("Password");
        if(emailId.equals("")){
            alertDialogBuild.setMessage("Your Password not found check your Entered mail Agin");
        }
        else{
            alertDialogBuild.setMessage("Your Password is: "+Password);
        }
        alertDialogBuild.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
       });
        AlertDialog alertDialog=alertDialogBuild.create();
        alertDialog.show();
        TextView textView=alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(15);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
