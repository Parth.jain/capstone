package com.example.android.capstone.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Struct;

/**
 * Created by parth on 19/2/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "UserDatabase.db";
    public static final String TABLE_NAME2="User_table";
    public static final String TABLE_NAME3="Booking_table";

    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME , null  , 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS Routes_table(ROUTE_ID INTEGER PRIMARY KEY AUTOINCREMENT,TYPE TEXT, BUS_NAME TEXT,FROM_CITY TEXT,TO_CITY TEXT,DEPART_TIME TIME,ARR_TIME TIME,FARE TEXT)");

        db.execSQL("CREATE TABLE IF NOT EXISTS "+TABLE_NAME2+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, BIRTHDATE TEXT, CITY TEXT, EMAIL TEXT, PASSWORD TEXT)");

        db.execSQL("CREATE TABLE "+TABLE_NAME3+"(B_ID INTEGER PRIMARY KEY AUTOINCREMENT,B_ROUTE_ID INTEGER,B_USER_ID INTEGER, DATE DATE,BOOKED_SEAT TEXT, NO_OF_SEATS TEXT,FARE TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Routes_table");

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME3);
    }

    //fetching bus details after search
    public Cursor fetchBusDetails(String from, String to) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT ROUTE_ID AS _id,* FROM Routes_table WHERE FROM_CITY=? AND TO_CITY=?", new String[]{from, to});
        return res;
    }
    //fetching bus details after search
    public Cursor fetchBusDetailstime(String from, String to) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT ROUTE_ID AS _id,* FROM Routes_table WHERE FROM_CITY=? AND TO_CITY=? AND (time(DEPART_TIME) BETWEEN date('now') AND '23:59')", new String[]{from, to});
        return res;
    }


    public Cursor getSingleBus(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " +"Routes_table" + " WHERE ROUTE_ID=?", new String[]{id});
        return cur;
    }

//insert new userdata
    public boolean insertData(String name, String birthday, String City, String Email, String Password) {

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("NAME",name);
        contentValues.put("BIRTHDATE",birthday);
        contentValues.put("CITY",City);
        contentValues.put("EMAIL",Email);
        contentValues.put("PASSWORD",Password);
        long result=db.insert(TABLE_NAME2,null,contentValues);
        if(result == -1){

            return false;
        }
        else{
            return true;
        }
    }

    public Cursor checkLogin(String email,String password){

        String[] columns={"ID"};
        SQLiteDatabase db=this.getWritableDatabase();

        String selection="EMAIL=? AND PASSWORD=?";

        String[] selectionArgs={email,password};

        Cursor cursor=db.query(TABLE_NAME2,columns,selection,selectionArgs,null,null,null);

        return  cursor;
    }
    public boolean checkUser(String email){

        String[] coloumns={"ID"};

        SQLiteDatabase db=this.getReadableDatabase();

        String selection= "EMAIL" +" =?";

        String[] selectionArgs={email};


        Cursor cursor=db.query(TABLE_NAME2,coloumns,selection,selectionArgs,null,null,null);

        int CursorCount=cursor.getCount();
        cursor.close();
        db.close();

        if(CursorCount>0){
            return true;
        }
        return false;
    }

    public String fetchPassword(String emailId) {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT "+"PASSWORD"+" FROM "+TABLE_NAME2+" WHERE "+"EMAIL"+"=?", new String[]{emailId});
        String password = null;
        if(cursor!=null && cursor.moveToFirst()){
            password=cursor.getString(cursor.getColumnIndex("PASSWORD"));
        }
        return password;
    }
    public String fetchUserName(String Id) {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT "+"NAME"+" FROM "+TABLE_NAME2+" WHERE "+"ID"+"=?", new String[]{Id});
        String Name= null;
        if(cursor!=null && cursor.moveToFirst()){
            Name=cursor.getString(cursor.getColumnIndex("NAME"));
        }
        return Name;
    }
    public String fetchEmailId(String id) {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT "+"EMAIL"+" FROM "+TABLE_NAME2+" WHERE "+"ID"+"=?", new String[]{id});
        String Email= null;
        if(cursor!=null && cursor.moveToFirst()){
            Email=cursor.getString(cursor.getColumnIndex("EMAIL"));
        }
        return Email;
    }
    //this will be used to display all data of user
    public Cursor getUserData(String id) {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cur=db.rawQuery("SELECT * FROM "+TABLE_NAME2+" WHERE ID=?",new String[]{id});
        return cur;

    }

    public boolean insertBookData(String ROUTE_ID, String userId, String date, String seat_no, Integer booking_seat, String FARE){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("B_ROUTE_ID",ROUTE_ID);
        contentValues.put("B_USER_ID",userId);
        contentValues.put("DATE",date);
        contentValues.put("BOOKED_SEAT",seat_no);
        contentValues.put("NO_OF_SEATS",booking_seat);
        contentValues.put("FARE",FARE);
        long result=db.insert(TABLE_NAME3,null,contentValues);
        if(result == -1){

            return false;
        }
        else{
            return true;
        }
    }

    //Fetch history Details
    public Cursor fetchHistory(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res=db.rawQuery("SELECT U.ID AS _id, U.NAME, B.DATE, R.FROM_CITY,R.TO_CITY, R.DEPART_TIME, R.ARR_TIME, R.BUS_NAME,R.TYPE, B.BOOKED_SEAT,B.FARE FROM Routes_table as R, Booking_table as B, User_table as U where U.ID=B.B_USER_ID AND B.B_ROUTE_ID=R.ROUTE_ID AND _id=? order by  B.B_ID desc",new String[]{id});
        return res;
    }


    public String getBookedSeat(String date, String routeID){
    SQLiteDatabase db=this.getReadableDatabase();
       Cursor cursor=db.rawQuery("SELECT group_concat(BOOKED_SEAT) FROM Booking_table where DATE=? AND  B_ROUTE_ID=?",new String[]{date,routeID});
    String Seats=null;
       if (cursor!=null && cursor.moveToFirst()){
           Seats=cursor.getString(cursor.getColumnIndex("group_concat(BOOKED_SEAT)"));
       }
       return Seats;
    }


    public boolean updateToDb(String id,String name,String birthdate,String city,String email,String password) {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("ID",id);
        contentValues.put("NAME",name);
        contentValues.put("BIRTHDATE",birthdate);
        contentValues.put("CITY",city);
        contentValues.put("EMAIL",email);
        contentValues.put("password",password);
        db.update(TABLE_NAME2,contentValues,"ID=?",new String[]{id});
        return true;
    }
}
