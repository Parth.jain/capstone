package com.example.android.capstone.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditProfileActivity extends AppCompatActivity {
    Calendar myCalendar = Calendar.getInstance();
    EditText mFullName,mDob,mEmail,mPassword;
    Spinner mCity;
    Button mUpdate;
    DatabaseHelper db;
    String Id,EmailId,FullName,City,Dob,Password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        db=new DatabaseHelper(this);
        Intent intent=getIntent();
        Id=intent.getStringExtra("Id");
        mCity=findViewById(R.id.spinnerCity);
        mFullName=findViewById(R.id.updateName);
        mDob=findViewById(R.id.updateBirthDate);
        mEmail=findViewById(R.id.updateEmailId);
        mPassword=findViewById(R.id.updatePassword);
        List<String> cities=new ArrayList<String>();
        cities.add("Ahmedabad");
        cities.add("Baroda");
        cities.add("Surat");
        cities.add("Rajkot");
        cities.add("Junagadh");
        ArrayAdapter<String> spinAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,cities);

        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        mCity.setPrompt("Pick One");

        mCity.setAdapter(spinAdapter);
        mCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position,
                                       long id) {
                City=parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final String name,email_id,city,birthdate,password;
        Cursor cur=db.getUserData(Id);

        if(cur!=null && cur.moveToFirst()){
            name=cur.getString(cur.getColumnIndex("NAME"));
            email_id=cur.getString(cur.getColumnIndex("EMAIL"));
            birthdate=cur.getString(cur.getColumnIndex("BIRTHDATE"));
            city=cur.getString(cur.getColumnIndex("CITY"));
            password=cur.getString(cur.getColumnIndex("PASSWORD"));
            mFullName.setText(name);
            mEmail.setText(email_id);
            mDob.setText(birthdate);
            mPassword.setText(password);
            int item = 0;
            if(city.equals("Ahmedabad")){
                item=0;
            }
            else if(city.equals("Baroda")){
                item=1;
            }
            else if(city.equals("Surat")){
                item=2;
            }
            else if(city.equals("Rajkot")){
                    item=3;
            }
            else if(city.equals("Junagadh")){
                item=4;
            }
            mCity.setSelection(item);
        }
        String alDate=mDob.getText().toString();
        final String[] datePart=alDate.split("/");
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth){
                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
               // updateLabel();
                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                Date date=myCalendar.getTime();
                if(date.after(Calendar.getInstance().getTime())){
                    Toast.makeText(EditProfileActivity.this, "Can not Select this Date As Birthdate, Please choose Another!", Toast.LENGTH_SHORT).show();
                    mDob.setText("");
                }
                else{
                    mDob.setText(sdf.format(myCalendar.getTime()));}

            }

        };
        mDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditProfileActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mUpdate=findViewById(R.id.btnUpdate);
        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FullName=mFullName.getText().toString();
                EmailId=mEmail.getText().toString();
                Dob=mDob.getText().toString();
                Password=mPassword.getText().toString();
                String newCity = mCity.getSelectedItem().toString();

                boolean valid=checkValidation(FullName,Dob,newCity,EmailId,Password);
                boolean emailValid=checkEmailValidation(EmailId);
                if(valid==true && emailValid==true)
                {
                    boolean updated= db.updateToDb(Id,FullName,Dob,newCity,EmailId,Password);
                    if(updated==true){
                        Intent profile=new Intent(EditProfileActivity.this,MyProfileActivity.class);
                        profile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(profile);
                        finish();
                    }
                }
                else{
                    Toast.makeText(EditProfileActivity.this, "Can not Update Data", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean checkEmailValidation(String emailId) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if(emailId.matches(emailPattern)){
            return true;
        }
        else {
            Toast.makeText(this, "Enter valid Email", Toast.LENGTH_SHORT).show();
            mEmail.setText("");
            return false;
        }
    }

    private boolean checkValidation(String fullName, String birthDate, String city,String emailId,String password) {
        if(!emailId.equals("") && !birthDate.equals("") && !password.equals("")&& !fullName.equals("") && !city.equals("")){
            return true;
        }
        else{

            return false;
        }
    }
}
