package com.example.android.capstone;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by parth on 12/2/18.
 */

public class SessionForCity {
    SharedPreferences cityPref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE=0;
    private static final String PREF_NAME="City_pref";
    private static final String FROM_CITY="From_City";

    private static final String TO_CITY="To_City";

    public SessionForCity(Context context){
        this.context=context;
        cityPref=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=cityPref.edit();
    }

    public void setFromCity(String fromCity){
        editor.putString(FROM_CITY,fromCity);

        editor.commit();

    }

    public void setToCity(String toCity){
        editor.putString(TO_CITY,toCity);
        editor.commit();

    }

    public void clearCity(){
        editor.clear();
        editor.commit();

    }
    public String getFromCity() {
        String city=cityPref.getString(FROM_CITY, null);
        return city;
    }
    public String getToCity(){
        String city=cityPref.getString(TO_CITY,null);
        return city;
    }

}
