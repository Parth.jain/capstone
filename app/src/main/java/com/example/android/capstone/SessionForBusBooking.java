package com.example.android.capstone;

import android.content.Context;
import android.content.SearchRecentSuggestionsProvider;
import android.content.SharedPreferences;

/**
 * Created by parth on 19/2/18.
 */

public class SessionForBusBooking {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    int PRIVATE_MODE=0;

    private static final String PREF_NAME="BUSBOOKING_PREF";

    private static final String ROUTE_ID="ROUTE_ID";


    private static final String DATE="DATE";

    private static final String FARE="FARE";

    public SessionForBusBooking(Context context){
        this.context=context;
        pref=context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor=pref.edit();
    }
    public void setRouteId(String routeId){
        editor.putString(ROUTE_ID,routeId);
        editor.commit();
    }
    public String getRouteID(){
        String Route_id=pref.getString(ROUTE_ID,null);
        return Route_id;

    }

    public void setFare(String fare){
        editor.putString(FARE,fare);
        editor.commit();
    }

    public String getFare(){
        String fare=pref.getString(FARE,null);
        return fare;
    }
    public void setDate(String date){
        editor.putString(DATE,date);
        editor.commit();
    }

    public String getDate(){
        String date=pref.getString(DATE,null);
        return date;
    }

}
