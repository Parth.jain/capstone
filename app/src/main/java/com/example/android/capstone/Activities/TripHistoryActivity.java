package com.example.android.capstone.Activities;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.capstone.Adapter.HistoryDetailAdapter;
import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionManager;

public class TripHistoryActivity extends AppCompatActivity {

    SessionManager mSesion;
    ListView mHistoryList;
    DatabaseHelper db;
    HistoryDetailAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_history);
        mSesion=new SessionManager(this);
        db=new DatabaseHelper(this);

        mHistoryList=findViewById(R.id.trip_history);
        Cursor cur=db.fetchHistory(mSesion.getUserId());
        if(cur.getCount()==0)
        {
            TextView textView=findViewById(R.id.nodata);
            textView.setVisibility(View.VISIBLE);
        }
        else {
            adapter = new HistoryDetailAdapter(this, db.fetchHistory(mSesion.getUserId()));
            mHistoryList.setAdapter(adapter);
        }
    }
}
