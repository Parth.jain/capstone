package com.example.android.capstone.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionForBusBooking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpperSeatFragment extends Fragment{
    DatabaseHelper dB;
    SessionForBusBooking mBusSession;

    List<String> BookedSeat=new ArrayList<String>();
    public UpperSeatFragment() {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View upper_view = inflater.inflate(R.layout.fragment_upper_seat, container, false);
        setRetainInstance(true);
        dB=new DatabaseHelper(getContext());
        mBusSession=new SessionForBusBooking(getContext());

        String bookedSeat=dB.getBookedSeat(mBusSession.getDate(),mBusSession.getRouteID());
        if(bookedSeat!=null) {
            BookedSeat= Arrays.asList(bookedSeat.split("\\s*,\\s*"));
        }
        Log.e("Booked seat",BookedSeat.toString());
        TableLayout t1;
        t1 = upper_view.findViewById(R.id.main_table_upper);
        int count = 1;
        char count2 = 'A' - 3;
        for (int i = 1; i <= 5; i++) {
            TableRow tr = new TableRow(getContext());
            TableLayout.LayoutParams params = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT);
            tr.setLayoutParams(params);
            for (int j = 1; j <= 4; j++) {
                Button seat_btn = new Button(getContext());
                TableRow.LayoutParams seat_params = null;
                seat_btn.setGravity(Gravity.CENTER);
                seat_btn.setPadding(0, 0, 0,0);
                if(j==3) {
                    seat_btn.setVisibility(View.INVISIBLE);
                }
                else if (i <= 2) {
                    seat_params = new TableRow.LayoutParams(140, 140);
                    seat_params.setMargins(30, 10, 0, 10);
                    seat_btn.setLayoutParams(seat_params);
                    seat_btn.setId(count);
                    seat_btn.setText(String.valueOf(count));
                    if(BookedSeat.contains(seat_btn.getText().toString())){
                     seat_btn.setBackgroundColor(getResources().getColor(R.color.confirm));
                    }
                    else
                    {
                        seat_btn.setBackgroundColor(getResources().getColor(R.color.remains));
                    }
                    count++;
                } else if (i > 2) {
                    seat_params = new TableRow.LayoutParams(140, 300);
                    seat_params.setMargins(30, 10, 0, 10);
                    seat_btn.setLayoutParams(seat_params);
                    seat_btn.setId(count2);
                    seat_btn.setText(String.valueOf(count2));
                    if(BookedSeat.contains(seat_btn.getText().toString())){
                        seat_btn.setBackgroundColor(getResources().getColor(R.color.confirm));
                    }
                    else
                    {
                        seat_btn.setBackgroundColor(getResources().getColor(R.color.remains));
                    }
                    count2++;
                }
                seat_btn.setOnClickListener((View.OnClickListener) this.getActivity());
                tr.addView(seat_btn);
            }
            count2 = (char) (count2 + 3);
            t1.addView(tr);
        }
        return upper_view;
    }


}
