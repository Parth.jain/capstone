package com.example.android.capstone.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionForCity;
import com.example.android.capstone.SessionManager;

public class MyProfileActivity extends AppCompatActivity {

    TextView mUsername,mEmailId,mUserCity,mBirthDate;
    SessionManager session;
    SessionForCity sessionForCity;
    Button mLogout,mEditProfile;
   DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myprofile);
        sessionForCity=new SessionForCity(this);

        db=new DatabaseHelper(this);
        mUsername=findViewById(R.id.Name);
        mEmailId=findViewById(R.id.EmailId);
        mUserCity=findViewById(R.id.City);
        mBirthDate=findViewById(R.id.Birthdate);
        session=new SessionManager(getApplicationContext());
        session.checkLogin();
        final String Id=session.getUserId();
        String name,email_id,city,birthdate;

        Cursor cur=db.getUserData(Id);
        if(cur!=null && cur.moveToFirst()){
            name=cur.getString(cur.getColumnIndex("NAME"));
            email_id=cur.getString(cur.getColumnIndex("EMAIL"));
            birthdate=cur.getString(cur.getColumnIndex("BIRTHDATE"));
            city=cur.getString(cur.getColumnIndex("CITY"));
            mUsername.setText(name);
            mBirthDate.setText(birthdate);
            mUserCity.setText(city);
            mEmailId.setText(email_id);

        }
        mEditProfile=findViewById(R.id.editProfileBtn);
        mEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MyProfileActivity.this,EditProfileActivity.class);
                intent.putExtra("Id",Id);

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //Work for Edit Profile User
            }
        });

        mLogout=findViewById(R.id.btnLogout);

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.LogoutUser();
                Intent startLogin=new Intent(MyProfileActivity.this,LoginActivity.class);
                sessionForCity.clearCity();
                startLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startLogin);
                finish();
            }
        });
    }

}
