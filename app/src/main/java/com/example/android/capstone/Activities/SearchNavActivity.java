package com.example.android.capstone.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionForBusBooking;
import com.example.android.capstone.SessionForCity;
import com.example.android.capstone.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SearchNavActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Button fromBtn,toBtn,searchBtn;
    EditText mFromLocation,mToLocation,mDateSelect;
    Calendar myCalendar = Calendar.getInstance();
    SessionForCity sessionForCity;
    SessionForBusBooking mBusSession;
    SessionManager userSession;
    DatabaseHelper userDb;
    TextView mUserName,mUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_nav);
        userSession=new SessionManager(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    View header=navigationView.getHeaderView(0);
        mUserName=(TextView)header.findViewById(R.id.user_name);
        mUserName.setText(userSession.getName().toString());

        mUserEmail=(TextView) header.findViewById(R.id.user_email);
        mUserEmail.setText(userSession.getEmail().toString());

        mBusSession=new SessionForBusBooking(this);
        sessionForCity=new SessionForCity(this);
        userDb=new DatabaseHelper(this);
        fromBtn=findViewById(R.id.pickFromCity);
        toBtn=findViewById(R.id.pickToCity);
        searchBtn=findViewById(R.id.searchBuses);

        mFromLocation=findViewById(R.id.editFromCity);

        mFromLocation.setText(sessionForCity.getFromCity());

        mToLocation=findViewById(R.id.editToCity);

        mToLocation.setText(sessionForCity.getToCity());

        fromBtn=findViewById(R.id.pickFromCity);

        toBtn=findViewById(R.id.pickToCity);

        fromBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(SearchNavActivity.this,MapsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("from","1");
                startActivity(intent);
            }
        });
        toBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SearchNavActivity.this,MapsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("to","1");
                startActivity(intent);

            }
        });
        mDateSelect=findViewById(R.id.datePick);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)           {
                view.setMinDate(System.currentTimeMillis() - 1000);
                myCalendar.set(Calendar.YEAR,year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                Date date=myCalendar.getTime();
                int toDate=date.getDate();
                int tomonth=date.getMonth();
                int toYear=date.getYear();
                if(date.after(Calendar.getInstance().getTime()) ||(toDate==(Calendar.getInstance().getTime().getDate())&& tomonth==Calendar.getInstance().getTime().getMonth() && toYear== Calendar.getInstance().getTime().getYear())){

                    mDateSelect.setText(sdf.format(myCalendar.getTime()));
                }
                else{
                    Toast.makeText(SearchNavActivity.this, "Can not Select this Date, Please choose Another!", Toast.LENGTH_SHORT).show();
                    mDateSelect.setText("");
                   }
            }
        };
        mDateSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SearchNavActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean valid= checkValidation(mFromLocation.getText().toString(),mToLocation.getText().toString(),mDateSelect.getText().toString());
                if(valid==true) {
                    Intent busIntent = new Intent(SearchNavActivity.this, BusDetailsActivity.class);
                    busIntent.putExtra("FROM",sessionForCity.getFromCity());
                    busIntent.putExtra("TO",sessionForCity.getToCity());
                    busIntent.putExtra("DATE",mDateSelect.getText().toString());
                    mBusSession.setDate(mDateSelect.getText().toString());
                    busIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(busIntent);
                }
            }
        });
    }
    private boolean checkValidation(String s, String s1, String s2) {
        if(s1.equals(s)){
            Toast.makeText(this, "Can not find bus for same destination and source location", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(s2.equals(""))
        {
            Toast.makeText(this, "Select Date first", Toast.LENGTH_SHORT).show();
            return false;
        }
        else{
            return true;
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.my_history) {
            Intent intent=new Intent(this,TripHistoryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (id == R.id.my_profile) {
            Intent intent=new Intent(this,MyProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (id == R.id.Logout) {
            userSession.LogoutUser();
            sessionForCity.clearCity();
            Intent startLogin=new Intent(SearchNavActivity.this,LoginActivity.class);
            startLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startLogin);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
