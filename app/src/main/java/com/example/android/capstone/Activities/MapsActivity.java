package com.example.android.capstone.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.android.capstone.R;
import com.example.android.capstone.SessionForCity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    SessionForCity sessionForCity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        sessionForCity=new SessionForCity(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.zoomTo(6.3f));
        mMap.getUiSettings().setAllGesturesEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng ahmedabad = new LatLng(	23.033863, 72.585022);
        mMap.addMarker(new MarkerOptions().position(ahmedabad).title("AHM"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ahmedabad));
        LatLng surat = new LatLng(	21.170240, 72.831062);
        mMap.addMarker(new MarkerOptions().position(surat).title("SUR"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(surat));
        LatLng baroda = new LatLng(	22.310696, 73.192635);
        mMap.addMarker(new MarkerOptions().position(baroda).title("BAR"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(baroda));
        LatLng rajkot = new LatLng(	22.308155, 70.800705);
        mMap.addMarker(new MarkerOptions().position(rajkot).title("RAJ"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(rajkot));
        LatLng junagadh = new LatLng(	21.515471, 70.456444);
        mMap.addMarker(new MarkerOptions().position(junagadh).title("JUN"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(junagadh));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(getIntent().hasExtra("from")) {
                    Intent intent = new Intent(MapsActivity.this, SearchNavActivity.class);
                    String location = marker.getTitle();
                    intent.putExtra("Location", location);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    sessionForCity.setFromCity(marker.getTitle());
                 }
                 else if(getIntent().hasExtra("to")){
                    Intent intent=new Intent(MapsActivity.this,SearchNavActivity.class);
                    String location = marker.getTitle();
                    intent.putExtra("Location", location);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    sessionForCity.setToCity(marker.getTitle());
                }
                return true;

            }
        });
    }
}
