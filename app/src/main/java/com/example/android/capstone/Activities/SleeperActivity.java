package com.example.android.capstone.Activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionForBusBooking;
import com.example.android.capstone.SessionManager;
import com.example.android.capstone.Adapter.ViewPagerAdapter;

import java.util.ArrayList;

public class SleeperActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<String> Seat=new ArrayList<String>();
    int selectedSeater=0,selectedSleeper=0;
    SessionForBusBooking mBusSession;
    SessionManager userSession;
    DatabaseHelper dB;
    Button mBook;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleeper);

        TabLayout tabLayout;
        ViewPager viewPager;
        ViewPagerAdapter viewPagerAdapter;
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

       mBusSession=new SessionForBusBooking(this);
        userSession=new SessionManager(this);
        dB=new DatabaseHelper(this);
        final String ROUTE_ID=mBusSession.getRouteID();
        String FARE=mBusSession.getFare();
        Log.e("Fare",FARE);
        String[] Fare=FARE.split("/");
        final String seaterFare=Fare[0];
        final String sleeperFare=Fare[1];
        Log.e("Seater fare",seaterFare);
        Log.e("Sleeper fare",sleeperFare);
        final int price_for_seater=Integer.parseInt(seaterFare);
        final int price_for_sleeper=Integer.parseInt(sleeperFare);

        mBook=findViewById(R.id.book_sleeper);
        mBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Booked Seat",Seat.toString());

                int fare=(selectedSeater*price_for_seater)+(selectedSleeper*price_for_sleeper);
         String Date=mBusSession.getDate();
                Integer booking_seat=Seat.size();
                if(!Seat.isEmpty()){
                    String Seat_no=Seat.toString().replace("[","").replace("]","").trim();

                    //String FARE=String.valueOf(booking_seat*price_Seat);
                    boolean Book= dB.insertBookData(ROUTE_ID,userSession.getUserId(),Date,Seat_no,booking_seat, String.valueOf(fare));
                    if(Book==true){
                        Intent intent1=new Intent(SleeperActivity.this,TripHistoryActivity.class);
                        startActivity(intent1);
                        finish();
                    }
                }
                else{
                    Toast.makeText(SleeperActivity.this, "Can not Book", Toast.LENGTH_SHORT).show();
                    Log.e("Sorry","Not Booked");
                }

            }
        });

    }
    public void onClick(View v) {
        int id = v.getId();
        Button b=(Button)v;
        String text=b.getText().toString();
        if(text.equals("1")||text.equals("2")|| text.equals("3")|| text.equals("4")|| text.equals("5")||text.equals("6")){
            selectedSeater++;
        }
        else{
            selectedSleeper++;
        }
        Log.e("Text",text);
        Log.e("id clicked", String.valueOf(id));
        ColorDrawable seatColor = (ColorDrawable) v.getBackground();
        int ColorId = seatColor.getColor();
        if (ColorId == getResources().getColor(R.color.select)) {
            v.setBackgroundColor(getResources().getColor(R.color.remains));
            Seat.remove(String.valueOf(text));
        }  else if(ColorId==getResources().getColor(R.color.confirm)){
            Toast.makeText(this,"Already booked",Toast.LENGTH_SHORT).show();
        }
        else{
            v.setBackgroundColor(getResources().getColor(R.color.select));
            Seat.add(String.valueOf(text));
        }
    }

}

