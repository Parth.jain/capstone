package com.example.android.capstone.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.capstone.Adapter.BusDetailAdapter;
import com.example.android.capstone.Model.DatabaseHelper;
import com.example.android.capstone.R;
import com.example.android.capstone.SessionForBusBooking;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BusDetailsActivity extends AppCompatActivity {

    ListView mBusList;
    DatabaseHelper busDb;
    TextView mBusDetailText;
    SessionForBusBooking mBusSession;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_details);
        mBusSession=new SessionForBusBooking(this);
        final Intent intent=getIntent();

        String from=intent.getStringExtra("FROM");
        String to=intent.getStringExtra("TO");
        String date=intent.getStringExtra("DATE");
        String finalDetailText=from+ " --> "+to+" Date: "+date;

        Date today= Calendar.getInstance().getTime();
        SimpleDateFormat fm=new SimpleDateFormat("dd/MM/yyyy");
        String Today=fm.format(today);
        Log.e("Today",Today);
        Cursor cur;

        mBusDetailText=findViewById(R.id.busDetailText);
        mBusDetailText.setText(finalDetailText);
        busDb=new DatabaseHelper(this);
        mBusList=findViewById(R.id.bus_list);
        BusDetailAdapter adapter;
        //=new BusDetailAdapter(this,busDb.fetchBusDetails(from,to));
        if(date.equals(Today)){
            //Log.e("bus","Notfound");
            cur=busDb.fetchBusDetailstime(from,to);
            adapter=new BusDetailAdapter(this,cur);
        }
        else{
            cur=busDb.fetchBusDetails(from,to);
            adapter=new BusDetailAdapter(this,cur);
        }
        mBusList.setAdapter(adapter);
        mBusList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Selected id",String.valueOf(id));

                Cursor cur=busDb.getSingleBus(String.valueOf(id));
                mBusSession.setRouteId(String.valueOf(id));
                String type = null;
                String fare=null;
                String Arr_time=null;
                String Depart_time=null;
                String Bus_name=null;

            if(cur.moveToFirst()) {
                    type = cur.getString(cur.getColumnIndex("TYPE"));
                    fare=cur.getString(cur.getColumnIndex("FARE"));
                    Arr_time=cur.getString(cur.getColumnIndex("ARR_TIME"));
                    Depart_time=cur.getString(cur.getColumnIndex("DEPART_TIME"));
                    Bus_name=cur.getString(cur.getColumnIndex("BUS_NAME"));
            }

            mBusSession.setFare(fare);
           if(type.equals("Seater")){
                Intent seatIntent=new Intent(BusDetailsActivity.this,
                        SeaterActivity.class);
                int price_seat=Integer.parseInt(fare);
                seatIntent.putExtra("price_seat",price_seat);
                seatIntent.putExtra("Bus_id",String.valueOf(id));
                startActivity(seatIntent);
            }
            else if(type.equals("Seater/Sleeper")){

                Intent seatIntent=new Intent(BusDetailsActivity.this,
                        SleeperActivity.class);
                startActivity(seatIntent);
            }

            }
        });

    }
}
