package com.example.android.capstone;

import android.app.Application;
import android.os.SystemClock;

/**
 * This Class is useful for Splash Screen Timer
 */


public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SystemClock.sleep(1000);
    }
}
